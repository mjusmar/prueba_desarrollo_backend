'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FinancieroSchema extends Schema {
  up () {
    this.create('financieros', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('salario', 254).notNullable()
      table.string('otros_ingresos', 254).notNullable()
      table.string('gastos_mensuales', 254).notNullable()
      table.string('gastos_financieros', 254).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('financieros')
  }
}

module.exports = FinancieroSchema
