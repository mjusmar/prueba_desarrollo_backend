'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResidenciaSchema extends Schema {
  up () {
    this.create('residencias', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('departamento', 254).notNullable()
      table.string('ciudad', 254).notNullable()
      table.string('barrio', 254).notNullable()
      table.string('direccion', 254).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('residencias')
  }
}

module.exports = ResidenciaSchema
