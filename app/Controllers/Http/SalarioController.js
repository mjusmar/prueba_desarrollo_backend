'use strict'

class SalarioController {
    async generate_salary({auth, request}){
        // const user = await auth.getUser();
        // console.log(user);
        const {salario} = request.all()
        
        var delta = 1000000;
        delta = (salario - (delta*2) > 0) ?  delta : salario/2;

        var max = salario + delta;
        var min = salario - delta;

        var salary_array = [ (min - delta) + '-' + min , min + '-' + max , max + '-' + (max + delta) ]

        salary_array.sort(()=> Math.random() - 0.5);

        const salary_response = {"a": salary_array[0], "b": salary_array[1], "c": salary_array[2]}
        
        return salary_response;
    }
}

module.exports = SalarioController
