'use strict'

const User = use('App/Models/User');
const Residencia = use('App/Models/Residencia')
const Financiero = use('App/Models/Financiero')

class UserController {

    async login({ request, auth }){
        const { personal_in } = request.all();
        const {email, password} = personal_in;
        const token = await auth.attempt(email,password);
        console.log('login ',token)
        return token;
    }
    async logout({auth, response}){
        const test =  await auth.revokeTokens();
        return response.send('ok');
    }

    async store({ request, response }) {
        const {personal_in, residencia_in, financiero_in} = request.all()
        const validate_email = await User.findBy('email',personal_in.email);
        
        if(validate_email!= null){
            return response.send("error");
        }
        try{
            const user = await User.create({
            nombre_completo: personal_in.nombre_completo,
            numero_id:       personal_in.numero_id,
            celular:         personal_in.celular,
            email:           personal_in.email,
            password:        personal_in.password,
            });
            
            const residencia = new Residencia()
            residencia.fill({
                departamento:   residencia_in.departamento,
                ciudad:         residencia_in.ciudad,
                barrio:         residencia_in.barrio,
                direccion:      residencia_in.direccion
            })
            await user.residencias().save(residencia)
            
            const financiero = new Financiero()
            financiero.fill({
                salario:            financiero_in.salario,
                otros_ingresos:     financiero_in.otros_ingresos,
                gastos_mensuales:   financiero_in.gastos_mensuales,
                gastos_financieros: financiero_in.gastos_financieros
            })
            await user.financieros().save(financiero)
            return this.login(...arguments);
        }
        catch(error){
            return response.send("error")
        }
    };
}

module.exports = UserController
