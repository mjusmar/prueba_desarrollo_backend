'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.group(() => {
  //ruta para registro, login y logout de usuarios
  Route.post('usuarios/registro', 'UserController.store');  
  Route.post('usuarios/login', 'UserController.login');    
  Route.post('usuarios/logout', 'UserController.logout').middleware('auth');    
  //ruta para generar rango de salarios
  Route.post('salario/generar', 'SalarioController.generate_salary').middleware('auth');    
}).prefix('api/v1');